from django.urls import path, include, reverse
from django.contrib import admin
from rest_framework.routers import DefaultRouter
from api import viewsets
from rest_framework.authtoken.views import obtain_auth_token

from django.shortcuts import redirect

production_router = DefaultRouter()
production_router.register(r'supply', viewsets.SupplyViewSet)


urlpatterns = [
    path('', lambda _: redirect('admin:index'), name='index'),
    path('', lambda request: redirect(reverse('api-root'))),
    path('api/consumo-gasolina/api-token-auth/', obtain_auth_token),
    path('admin/', admin.site.urls),
    path('api/consumo-gasolina/', include(production_router.urls)),
]
